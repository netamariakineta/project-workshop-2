package config

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

const (
	username = "root"
	password = ""
	hostname = "127.0.0.1:3306"
	dbname   = "workshop1"
)

func OpenConnection() *sql.DB {
	db, err := sql.Open("mysql", username+":"+password+"@tcp("+hostname+")/"+dbname)

	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	return db
}
