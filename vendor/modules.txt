# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
# github.com/go-playground/locales v0.14.0
## explicit; go 1.13
github.com/go-playground/locales
github.com/go-playground/locales/currency
# github.com/go-playground/universal-translator v0.18.0
## explicit; go 1.13
github.com/go-playground/universal-translator
# github.com/go-playground/validator/v10 v10.11.0
## explicit; go 1.13
github.com/go-playground/validator/v10
# github.com/go-sql-driver/mysql v1.7.0
## explicit; go 1.13
github.com/go-sql-driver/mysql
# github.com/golang-jwt/jwt/v4 v4.4.2
## explicit; go 1.16
# github.com/labstack/echo v3.3.10+incompatible
## explicit
github.com/labstack/echo
# github.com/labstack/gommon v0.3.1
## explicit; go 1.12
github.com/labstack/gommon/color
github.com/labstack/gommon/log
# github.com/leodido/go-urn v1.2.1
## explicit; go 1.13
github.com/leodido/go-urn
# github.com/lib/pq v1.10.6
## explicit; go 1.13
github.com/lib/pq
github.com/lib/pq/oid
github.com/lib/pq/scram
# github.com/mattn/go-colorable v0.1.12
## explicit; go 1.13
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.14
## explicit; go 1.12
github.com/mattn/go-isatty
# github.com/stretchr/testify v1.7.2
## explicit; go 1.13
# github.com/valyala/bytebufferpool v1.0.0
## explicit
github.com/valyala/bytebufferpool
# github.com/valyala/fasttemplate v1.2.1
## explicit; go 1.12
github.com/valyala/fasttemplate
# golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
## explicit; go 1.17
golang.org/x/crypto/acme
golang.org/x/crypto/acme/autocert
golang.org/x/crypto/sha3
# golang.org/x/net v0.0.0-20220728211354-c7608f3a8462
## explicit; go 1.17
golang.org/x/net/idna
# golang.org/x/sys v0.0.0-20220731174439-a90be440212d
## explicit; go 1.17
golang.org/x/sys/cpu
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
# golang.org/x/text v0.3.7
## explicit; go 1.17
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/language
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
