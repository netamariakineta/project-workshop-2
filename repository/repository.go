package repository

import (
	"log"
	"project-workshop-2/config"
	"project-workshop-2/models"

	_ "github.com/lib/pq"
)

func POSTInsertUser(user models.RequestAddUser) (err error) {
	db := config.OpenConnection()

	_, err = db.Query(`INSERT INTO users(users_name, users_username,address, phone, email) VALUES (?, ?, ?, ?, ?)`, user.UsersName, user.UsersUsername, user.Address, user.Phone, user.Email)
	if err != nil {
		log.Println("ERROR QUERYING INSERT USERS: ", err.Error())
		return
	}
	return
}

func GETAllUsers() (response []models.Users, err error) {
	db := config.OpenConnection()

	query := `SELECT id, users_name, users_username, address, phone, email FROM users`
	rows, err := db.Query(query)
	if err != nil {
		log.Println("ERROR QUERYING GET ALL USERS: ", err.Error())
		return
	}
	for rows.Next() {
		var scan models.Users
		rows.Scan(&scan.Id, &scan.UsersName, &scan.UsersUsername, &scan.Address, &scan.Phone, &scan.Email)
		response = append(response, scan)
	}
	return
}
