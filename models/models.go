package models

type Users struct {
	Id            int    `json:"id"`
	UsersName     string `json:"usersName"`
	UsersUsername string `json:"usersUsername"`
	Address       string `json:"address"`
	Phone         string `json:"phone"`
	Email         string `json:"email"`
}
type RequestAddUser struct {
	UsersName     string `json:"usersName" validate:"required"`
	UsersUsername string `json:"usersUsername" validate:"required"`
	Address       string `json:"address"`
	Phone         string `json:"phone"`
	Email         string `json:"email" validate:"required,email"`
}

type Response struct {
	ErrMsg string      `json:"errMsg"`
	Status bool        `json:"status"`
	Result interface{} `json:"result"`
}
