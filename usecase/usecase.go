package usecase

import (
	"project-workshop-2/models"
	"project-workshop-2/repository"
	. "project-workshop-2/utils"

	"fmt"
	"log"

	"encoding/json"
	"net/http"

	"github.com/labstack/echo"
	_ "github.com/lib/pq"
)

//c := echo.New()

func GETDataUser(c echo.Context) error {
	var respon models.Response
	usera, err := repository.GETAllUsers()

	userBytes, _ := json.MarshalIndent(usera, "", "\t")
	fmt.Println(string(userBytes))

	//c.JSON(http.StatusOK, userBytes)
	//w.Write(userBytes)

	if err != nil {
		log.Fatal(err)
		respon.Status = false
		respon.ErrMsg = "Gagal mendapatkan list pengguna"
		respon.Result = ""

	}
	respon.Status = true
	respon.ErrMsg = "List data pengguna berhasil diakses"
	respon.Result = usera
	return c.JSON(http.StatusCreated, respon)

}

func InsertUserData(c echo.Context) (err error) {
	var request models.RequestAddUser
	var response models.Response
	err = c.Bind(&request)
	if err != nil {
		fmt.Println(err)
	}
	err = ValidateStruct(request)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	err = repository.POSTInsertUser(request)
	if err != nil {
		fmt.Println(err)
		response.Status = false
		response.ErrMsg = "Error input data baru"
		response.Result = err

	} else {
		response.Status = true
		response.ErrMsg = "BERHASIL INPUT DATA"
		response.Result = err

	}

	fmt.Println(response)
	// w.WriteHeader(http.StatusOK)
	// w.Write([]byte(respon.Messages))
	return c.JSON(http.StatusCreated, response)
}
