package utils

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

func ValidateStruct(e interface{}) error {

	fmt.Println("Cek Validation")

	var validate *validator.Validate
	validate = validator.New()
	err := validate.Struct(e)
	if err != nil {
		return err
	}
	return nil
}
