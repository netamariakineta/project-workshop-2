package main

import (
	"project-workshop-2/usecase"

	"github.com/labstack/echo"
)

func Router(c *echo.Echo) {
	c.GET("/getall", usecase.GETDataUser)
	c.POST("/add", usecase.InsertUserData)

}

func main() {
	c := echo.New()
	Router(c)

	c.Logger.Fatal(c.Start(":8088"))
}
